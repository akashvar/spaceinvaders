#include "bulletcreator.h"

BulletCreator::BulletCreator()
{

}

//create and return pointer to a new angledBullet object
AngledBullet *BulletCreator::BulletCreator::createAngledBullet(int x, int y, int yMover, int xMover)
{
    return new AngledBullet(x, y, yMover, xMover);
}

//create and return pointer to new Bullet object
Bullet *BulletCreator::BulletCreator::createSimpleBullet(int x, int y, int mover)
{
    return new Bullet(x, y, mover);
}
