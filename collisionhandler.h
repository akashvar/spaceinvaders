/*
 * collisionhandler.h
 * Class header for the class that is responsible for detecting
 * collisions between various *Game Elements* and whatever must
 * happen after the collisions.
 *
 */

#ifndef COLLISIONHANDLER_H
#define COLLISIONHANDLER_H
#include <QRect>
#include "activebullets.h"
#include "spaceship.h"
#include "aliengrid.h"

class CollisionHandler : public QRect
{
public:
    CollisionHandler();

    /*
     * Checks if any bullet on screen has collided with any other *GameElement*
     */
    int checkCollisions(ActiveBullets *&Bullets, AlienGrid *&aliengrid, Spaceship *&ship);
};

#endif // COLLISIONHANDLER_H
