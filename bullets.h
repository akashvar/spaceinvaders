/*
 * Interface that all types of bullets in the game
 * must conform to.
 *
 */

#ifndef BULLETS_H
#define BULLETS_H

#include <QPixmap>

class Bullets
{
public:

    /*
     * Constructor
     */

    Bullets(int x = 50, int y = 0, int yMover = 5): m_xPos(x), m_yPos(y), m_yMover(yMover) {}


    virtual ~Bullets(){}

    /*
     * Gets the image of the bullet object
     */
    virtual QPixmap getBulletImage() const { return this->m_bulletImage; }

    /*
     * Pure virtual function that moves the bullet object accoriding to the type it is
     */
    virtual void moveBullet() = 0;

    /*
     * Gets the x coordindate of the bullet object
     */
    virtual int getX() { return this->m_xPos; }

    /*
     * Gets the y coordindate of the bullet object
     */
    virtual int getY() { return this->m_yPos; }

    /*
     * Gets the movement unit along y axis for the bullet object
     */
    virtual int getYMover() { return this->m_yMover; }

protected:
    int m_xPos; //x coord of the bullet
    int m_yPos; //y coord of the bullet
    int m_yMover; //movement unit along y axis
    QPixmap m_bulletImage; //image of the bullet
};

#endif // BULLETS_H
