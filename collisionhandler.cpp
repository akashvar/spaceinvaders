/*
 * collisionhandler.cpp
 * Class implementation for the class that is responsible for detecting
 * collisions between various *Game Elements* and whatever must
 * happen after the collisions.
 *
 */

#include "collisionhandler.h"
#include <iostream>
#include <QRect>

CollisionHandler::CollisionHandler()
{

}

/*
 * Checks collisions between all bullets, aliens and the spaceship and returns the number
 * of aliens killed.
 */
int CollisionHandler::checkCollisions(ActiveBullets* &bullets, AlienGrid* &aliengrid, Spaceship *&ship)
{
    int score = 0; //number of aliens killed currently
    int bulletSize = bullets->getSize(); //number of bullets on screen
    int alienSize = aliengrid->getSize(); //number of aliens on screen

    //QRect object with same coordinates as the ship
    QRect* shipRect = new QRect(ship->getX(), ship->getY(), ship->getSpaceshipImage().width(), ship->getSpaceshipImage().height());

    for (int i = 0; i < bulletSize; i++) {

        Bullets* currentBullet = bullets->getActiveBullet(i);

        //QRect object with same coordinates as the current bullet
        QRect* bulletRect = new QRect(currentBullet->getX(), currentBullet->getY(), currentBullet->getBulletImage().width(), currentBullet->getBulletImage().height());

        //positive mover implies bullet shot by aliens
        if (currentBullet->getYMover() > 0) {
            if (bulletRect->intersects(*shipRect)) {

                //Free the memory
                delete bulletRect;
                delete shipRect;
                std::cout << "Game over!\n" << std::endl;
                exit(1);

            }
        }

        //check collision of current bullet with every alien
        for (int j = 0; j < alienSize; j++) {

            Alien* currentAlien = aliengrid->getAlien(j);

            //QRect object with same coordinates as the current alien
            QRect* alienRect = new QRect(currentAlien->getX(), currentAlien->getY(), currentAlien->getAlienImage().width(), currentAlien->getAlienImage().height());

            //negative mover implies bullet shot by spaceship
            if (currentBullet->getYMover() < 0) {
                if (bulletRect->intersects(*alienRect)) {

                    //Free the memory
                    delete bullets->getActiveBullet(i);
                    delete aliengrid->getAlien(j);

                    //Erase the empty position(s) in the vector
                    aliengrid->getAliens().erase(aliengrid->getAliens().begin() + j);
                    bullets->getBullets().erase(bullets->getBullets().begin() + i);

                    bulletSize--;
                    alienSize--;
                    score++;
                    break;
                }
            }
        }
        delete bulletRect;
    }
    delete shipRect;
    return score;
}



