/*
 * aliengrid.h
 * Class header for an alien grid that is a subclass of
 * Game Elements. It has minX, maxX and minY, maxY coordinates
 * that correspond to the smallet and the largest x and y
 * coordinates of the grid respectively. It also has two ints, the number
 * of rows and columns it has as members.
 *
 */

#ifndef ALIENGRID_H
#define ALIENGRID_H
#include "gameelement.h"
#include "alien.h"
#include "bullet.h"
#include "activebullets.h"
#include <vector>

class AlienGrid : public GameElement
{
public:
    /*
     ***** CONSTRUCTOR *****
     */
    AlienGrid(int rows, int cols);

    /*
     * Move the grid along the x axis by x units
     */
    void moveX(int x);

    /*
     * Move the grid along the y axis by y units
     */
    void moveY(int y);

    /*
     * Get the number of aliens left in grid
     */
    size_t getSize();

    /*
     * Check if alien grid is empty
     */
    bool isEmpty();

    /*
     * Get the vector storing pointers to alien objects
     */
    std::vector<Alien *> &getAliens();

    /*
     * Get the biggest x coord out of all the aliens
     */
    int getMaxX();

    /*
    * Get the smallest x coord out of all the aliens
    */
    int getMinX();

    /*
     * Get the smallest y coord out of all the aliens
     */
    int getMinY();

    /*
     * Get the biggest y coord out of all the aliens
     */
    int getMaxY();

    /*
     * Return alien at passed index in the grid
     */
    Alien *getAlien(int index);


private:
    std::vector<Alien*> m_aliens; //vector storing all alive aliens in the grid

    //dimensions of the alien grid
    int m_rows;
    int m_cols;

};

#endif // ALIENGRID_H
