/*
 * aliengrid.cpp
 * Class implementation for an alien grid that is a subclass of
 * Game Elements. It has minX, maxX and minY, maxY coordinates
 * that correspond to the smallet and the largest x and y
 * coordinates of the grid respectively. It also has two ints, the number
 * of rows and columns it has as members.
 */

#include "aliengrid.h"
#include <iostream>

//Creates new alien grid with all the aliens part of it
AlienGrid::AlienGrid(int rows, int cols) : m_rows(rows), m_cols(cols)
{
    int alienX = 30;
    int alienY = 30;

    for (int i = 0; i < m_rows; i++) {
        for (int j = 0; j < m_cols; j++) {

            m_aliens.push_back(new Alien(alienX, alienY));
            alienX = alienX + 60;

            if (i == 0) {
                this->m_aliens.at(i * cols + j)->setImage();

            } else if (i == 1) {
                this->m_aliens.at(i * cols + j)->setImage();

            } else {
                this->m_aliens.at(i * cols + j)->setImage();
            }
        }
        alienY = alienY + 60;
        alienX = 30;
    }
}

//Gets the smallest x coordinate out of all the aliens in grid
int AlienGrid::getMinX()
{
    int minX = 20000;
    for (size_t i = 0; i < this->getSize(); i++) {
        if (this->getAlien(i)->getX() < minX) minX = this->getAlien(i)->getX();
    }
    return minX;
}

//Gets the biggest x coordinate out of all the aliens in grid
int AlienGrid::getMaxX()
{
    int maxX = -1;
    for (size_t i = 0; i < this->getSize(); i++) {
        if (this->getAlien(i)->getX() > maxX) maxX = this->getAlien(i)->getX();
    }
    return maxX;
}

//Gets the smallest y coordinate out of all the aliens in grid
int AlienGrid::getMinY()
{
    int minY = 20000;
    for (size_t i = 0; i < this->getSize(); i++) {
        if (this->getAlien(i)->getY() < minY) minY = this->getAlien(i)->getY();
    }
    return minY;
}

//Gets the biggest y coordinate out of all the aliens in grid
int AlienGrid::getMaxY()
{
    int maxY = -1;
    for (size_t i = 0; i < this->getSize(); i++) {
        if (this->getAlien(i)->getY() > maxY) maxY = this->getAlien(i)->getY();
    }
    return maxY;
}

//Returns the alien at passed index in the grid
Alien *AlienGrid::getAlien(int index)
{
    if ((size_t)index >= this->getSize())
    {
        std::cout << "Invalid index error for alien grid!" << std::endl;
        exit(1);
    }
    return this->m_aliens.at(index);
}

//Moves the alien grid along the x axis by the passed value
void AlienGrid::moveX(int x)
{
    for (size_t i = 0; i < this->m_aliens.size(); i++) {

        this->m_aliens.at(i)->moveAlienX(x);
    }
}

//Moves the alien grid along the y axis by the passed value
void AlienGrid::moveY(int y)
{
    for (size_t i = 0; i < this->m_aliens.size(); i++) {

        this->m_aliens.at(i)->moveAlienY(y);
    }
}

//Gets the size of the alien grid
size_t AlienGrid::getSize()
{
    return this->m_aliens.size();
}

//Checks if vector storing the alien pointers is empty or not
bool AlienGrid::isEmpty()
{
    return this->m_aliens.empty();
}

//Gets the vector storing alien pointers
std::vector<Alien *>& AlienGrid::getAliens()
{
    return this->m_aliens;
}
