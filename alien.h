/*
 * alien.h
 * Class header for an alien that is a subclass of
 * Game Elements. It has x, y coordinates and an image (.png)
 *
 */

#ifndef ALIEN_H
#define ALIEN_H

#include "gameelement.h"
#include <QString>
#include <QPixmap>


class Alien : public GameElement
{
public:

    /*
     * Creates an alien with passed x and y coords as initial positions
     */
    Alien(int x, int y);

    /*
     * Moves the alien along the x axis by moveX units
     */
    void moveAlienX(int moveX);

    /*
     * Moves the alien along the y axis by moveY units
     */
    void moveAlienY(int moveY);

    /*
     * Sets the image of the alien
     */
    void setImage();

    /*
     * Gets the image of the alien
     */
    QPixmap getAlienImage() const;


private:
    QPixmap m_alienimage; //The image of the alien
};

#endif // ALIEN_H
