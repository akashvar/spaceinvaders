/*
 * angledbullet.cpp
 * Class implementation for a bullet that is a subclass of
 * Game Elements. Such bullets travel on a diagonal path.
 * Each instance is given x,y coordinates relative to
 * the spaceship position at the time of instantiation
 * Author: Anon
 */

#include "bullets.h"
#include "angledbullet.h"

AngledBullet::AngledBullet(int xPos, int yPos, int yMover, int xMover): m_xMover(xMover)
{

    //String of file name relative to makefile in the release build
    QPixmap tempBullet("images/Bullet.png");
    m_bulletImage = tempBullet.scaled(QSize(20,20));
    Bullets::m_xPos = xPos;
    Bullets::m_yPos = yPos;
    Bullets::m_yMover = yMover;
}

//Destructor
AngledBullet::~AngledBullet()
{

}

//Gets the image of the bullet to be rendered in the game universe
/**QPixmap AngledBullet::getBulletImage() const
{
    return m_bulletImage;
}*/

//Moves the bullets along the x and y axes
void AngledBullet::moveBullet()
{
    m_yPos += m_yMover;
    m_xPos += m_xMover;
}




