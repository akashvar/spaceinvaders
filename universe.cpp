/*
 * universe.cpp
 * Class implementation for game universe that is a subclass of QDialog.
 * The universe dialog creates a game configuration objects, a spaceship object, an
 * active bullets object and three starfields that scroll through the background.
 * Every 20msec the frame is refreshed showing the movement of these game elements
 * and background objects. The instructions for movement are within saved to the
 * GameConfiguration object by the IOFile object that also saves the last x coordinate
 * of the spaceship back to the config.ini file in the destructor.
 * Author: Anon
 */

#include "iofile.h"
#include "farstarfieldbuilder.h"
#include "gameconfiguration.h"
#include "globaldimensions.h"
#include "midstarfieldbuilder.h"
#include "nearstarfieldbuilder.h"
#include "starfieldbuilder.h"
#include "starfieldcreator.h"
#include "universe.h"
#include <QKeyEvent>
#include <QFont>
#include <Qt>
#include <QString>
#include <QTimer>


namespace universe { //start namespace

    //Constructor has a default QWidget value of nullptr
    Universe::Universe(QWidget *parent) : QDialog(parent)
    {
        //READ CONFIG FILE
        //Create a file processing object to read and write to the config object
        IOFile iofile(m_config);

        //Read the config file
        iofile.readFile();


        //CREATE AND ADJUST SPACESHIP
        //Pointer to an instance of a spaceship object
        int xPosition = m_config.getStartingXPos();
        int yPosition = globalHeight - globalPadding;
        string size = m_config.getSpaceshipSize();
        m_spaceship = new Spaceship(xPosition, yPosition, size);

        //Adjust y position for spaceship height
        int adjustY = -1 * m_spaceship->getSpaceshipImage().height();
        m_spaceship->moveSpaceshipY(adjustY);

        //Create the grid of aliens
        m_aliengrid = new AlienGrid(4, 10);

        //Create the active bullets object
        m_bullets = new ActiveBullets();

        //CREATE SCROLLING STARS
        //initialise background stars
        StarfieldBuilder *builder1 = new FarStarfieldBuilder;
        StarfieldCreator creator1(builder1);
        m_farStarfield = creator1.createStarfield();

        //initialise midground stars
        StarfieldBuilder *builder2 = new MidStarfieldBuilder;
        StarfieldCreator creator2(builder2);
        m_midStarfield = creator2.createStarfield();

        //initialise foreground stars
        StarfieldBuilder *builder3 = new NearStarfieldBuilder;
        StarfieldCreator creator3(builder3);
        m_nearStarfield = creator3.createStarfield();

        //delete builders to free memory
        delete builder1;
        delete builder2;
        delete builder3;

        //initialise the collision handler object
        m_collisionHandler = new CollisionHandler();


        //BACKGROUND SETUP
        setStyleSheet("background-color: #000000;");
        this->resize(globalWidth, globalHeight);
        update();

        //TIMER SETUP
        srand(time(NULL));
        QTimer *timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(nextFrame()));
        timer->start(20); //20msec
    }

    //Destructor saves the last x coordinate of the spaceship to config.ini
    //and deletes the spaceship and starfield to free memory
    Universe::~Universe()
    {
        IOFile iofile(m_config);
        iofile.writeFile(m_spaceship->getX());
        delete m_spaceship;
        delete m_farStarfield;
        delete m_midStarfield;
        delete m_nearStarfield;
        delete m_collisionHandler;
        delete m_aliengrid;
    }

    //Paints the graphics in the dialog
    void Universe::paintEvent(QPaintEvent *event)
    {
        QPainter painter(this);

        //PAINT STARS
        //Paint background starfield
        painter.setPen(QPen(m_farStarfield->getColour(), 1));
        for (int i = 0; i < m_farStarfield->getSize(); i++)
        {
            painter.drawPoint(QPoint(m_farStarfield->getX(i), m_farStarfield->getY(i)));
        }

        //Paint midground starfield
        painter.setPen(QPen(m_midStarfield->getColour(), 1));
        for (int i = 0; i < m_midStarfield->getSize(); i++)
        {
            painter.drawPoint(QPoint(m_midStarfield->getX(i), m_midStarfield->getY(i)));
        }

        //Paint foreground starfield
        painter.setPen(QPen(m_nearStarfield->getColour(), 1));
        for (int i = 0; i < m_nearStarfield->getSize(); i++)
        {
            painter.drawPoint(QPoint(m_nearStarfield->getX(i), m_nearStarfield->getY(i)));
        }

        //Draw the score of the player on the screen
        QString score = "Score: " + QString::number(m_scoreCount);
        painter.setFont(QFont("Arial", 16, QFont::Bold));
        painter.setPen(Qt::yellow);
        painter.drawText(10, 20, score);

        //PAINT ACTIVE BULLETS
        //Draw any active bullets using the object (x, y) coordinates
        if (!m_bullets->getBullets().empty()) {
            int size = m_bullets->getSize();

            for (int i = 0; i < size; i++)
            {
                Bullets *currentBullet = m_bullets->getActiveBullet(i);
                painter.drawPixmap(currentBullet->getX(), currentBullet->getY(), currentBullet->getBulletImage());
            }
        }

        //PAINT SPACESHIP
        //Draw the spaceship using the object (x, y) coordinates
        painter.drawPixmap(m_spaceship->getX(), m_spaceship->getY(), m_spaceship->getSpaceshipImage());

        //PAINT ALIEN GRID
        //using the x and y coords of the aliens
        if (!m_aliengrid->isEmpty()) {
            int size = m_aliengrid->getSize();

            for(int i = 0; i < size; i++)
            {
                Alien *currentAlien = m_aliengrid->getAlien(i);
                painter.drawPixmap(currentAlien->getX(), currentAlien->getY(), currentAlien->getAlienImage());
            }
        }
    }

    //Responds to keys pressed during the game
    void Universe::keyPressEvent(QKeyEvent *event)
    {
        //Red spaceship if '1' is pressed
        if (event->key() == Qt::Key_1)
        {
            m_spaceship->updateSpaceshipColour(1);
        }

        //Blue spaceship if '2' is pressed
        else if (event->key() == Qt::Key_2)
        {
            m_spaceship->updateSpaceshipColour(2);
        }

        //Purple spaceship if '3' is pressed
        else if (event->key() == Qt::Key_3)
        {
            m_spaceship->updateSpaceshipColour(3);
        }

        //Green spaceship if '4' is pressed
        else if (event->key() == Qt::Key_4)
        {
            m_spaceship->updateSpaceshipColour(4);
        }
    }

    //Alters the coordinates of objects active in the dialog
    //and updates the frame
    void Universe::nextFrame()
    {

        int numOfDirections = int(m_config.getDirections().size());
        int numOfAlienDirections = int(m_config.getAlienDirections().size());
        int xMax = globalWidth - globalPadding - m_spaceship->getSpaceshipImage().width() - 1;
        int xMin = globalPadding;

        //If there are more instructions to read, continue to read instructions
        if (m_directionsIndex < numOfDirections)
        {
            m_readingInstructions = true;
        }

        //Else, read no further instructions and use default movements
        else
        {
            m_readingInstructions = false;
        }

        //If there are more alien instructions to read, continue to read instructions
        if (m_alienDirectionsIndex < numOfAlienDirections)
        {
            m_readingAlienInstructions = true;
        }

        //Else, read no further instructions and use default movements
        else
        {
            m_readingAlienInstructions = false;
        }

        string instruction;
        string alienInstruction;

        //check if config file had alien instructions
        if (m_readingAlienInstructions) {

            alienInstruction = m_config.getAlienDirections().at(m_alienDirectionsIndex);

            //MOVE ALIENGRID RIGHT
            if (alienInstruction.compare("right") == 0)
            {
                if (m_aliengrid->getMaxX() <= xMax)
                {
                    m_aliengrid->moveX(m_alienMoverX);
                }
            }

            //MOVE ALIENGRID LEFT
            else if (alienInstruction.compare("left") == 0)
            {
                if (m_aliengrid->getMinX() >= xMin)
                {
                    m_aliengrid->moveX(m_alienMoverX * -1);
                }
            }

            //MOVE ALIENGRID UP
            else if (alienInstruction.compare("up") == 0)
            {
                if (m_aliengrid->getMinY() >= 0)
                {
                    m_aliengrid->moveY(m_alienMoverY * -1);
                }
            }

            //MOVE ALIENGRID DOWN
            else if (alienInstruction.compare("down") == 0)
            {
                if (m_aliengrid->getMaxY() <= m_spaceship->getY())
                {
                    m_aliengrid->moveY(m_alienMoverY);
                }
            }

            //The pixel interval is the length of pixels the ship and aliens can travel across in the x-axis,
            //divided by 10. Each left/right instruction will move the ship across a tenth of this space.
            int pixelInterval = int((globalWidth - (globalPadding * 2) - m_aliengrid->getAlien(0)->getAlienImage().width()) / 9);

            //The spaceship and aliens move by 5px each update, so to move across a pixel interval within
            //one timestep, divide by 5.
            int timestep = pixelInterval / 3;

            //If the timestep is a factor of the number of updates, read a new instruction
            if ((m_timestepCounter % timestep == 0))
            {
                m_alienDirectionsIndex++;

                //Choose a random alien out of the grid and shoot
                int i = this->generateRandom(0, this->m_aliengrid->getSize());

                int bulletX = m_aliengrid->getAlien(i)->getX() + (m_aliengrid->getAlien(i)->getAlienImage().width() / 2);
                int bulletY = m_aliengrid->getAlien(i)->getY() + m_aliengrid->getAlien(i)->getAlienImage().height();
                std::string element = "alien";
                m_bullets->addBullet(bulletX, bulletY, element);

            }

            m_timestepCounter++;

        } else
        {

            //Move the aliens in a fixed trajectory
            //MOVE ALIENGRID
            if (m_aliengrid->getMaxX() >= xMax)
            {
                m_alienMoverX *= -1;
                m_aliengrid->moveY(20);

            }
            else if (m_aliengrid->getMinX() <= xMin)
            {
                m_alienMoverX *= -1;
                m_aliengrid->moveY(20);
            }
            m_aliengrid->moveX(m_alienMoverX);

            if (m_spaceship->getX() % 200 == 0) {
                int i = this->generateRandom(0, this->m_aliengrid->getSize());

                int bulletX = m_aliengrid->getAlien(i)->getX() + (m_aliengrid->getAlien(i)->getAlienImage().width() / 2);
                int bulletY = m_aliengrid->getAlien(i)->getY() + m_aliengrid->getAlien(i)->getAlienImage().height();
                std::string element = "alien";
                m_bullets->addBullet(bulletX, bulletY, element);

            }
        }

        //READING CONFIGURATION DIRECTIONS - provided in config.ini
        //If reading instructions has been set to true
        if (m_readingInstructions)
        {

            instruction = m_config.getDirections().at(m_directionsIndex);


            //SHOOT BULLET AND FOLLOW NEXT INSTRUCTION
            if (instruction.compare("shoot") == 0)
            {
                int bulletX = m_spaceship->getX() + (m_spaceship->getSpaceshipImage().width() / 2);
                int bulletY = m_spaceship->getY();
                std::string element = "ship";
                m_bullets->addBullet(bulletX, bulletY, element);

                //If shoot wasn't the last instruction, find the next ship movement instruction
                if ((m_directionsIndex + 1) < numOfDirections)
                {
                    m_directionsIndex++;

                    instruction = m_config.getDirections().at(m_directionsIndex);

                }

            }

            //MOVE SPACESHIP RIGHT
            if (instruction.compare("right") == 0)
            {
                if (m_spaceship->getX() <= xMax)
                {
                    m_spaceship->moveSpaceshipX(m_spaceshipMover);
                }
            }

            //MOVE SPACESHIP LEFT
            else if (instruction.compare("left") == 0)
            {
                if (m_spaceship->getX() >= xMin)
                {
                    m_spaceship->moveSpaceshipX(m_spaceshipMover * -1);
                }
            }

            //The pixel interval is the length of pixels the ship and aliens can travel across in the x-axis,
            //divided by 10. Each left/right instruction will move the ship across a tenth of this space.
            int pixelInterval = int((globalWidth - (globalPadding * 2) - m_spaceship->getSpaceshipImage().width()) / 10);

            //The spaceship and aliens move by 5px each update, so to move across a pixel interval within
            //one timestep, divide by 5.
            int timestep = pixelInterval / 5;

            //If the timestep is a factor of the number of updates, read a new instruction
            if ((m_timestepCounter % timestep == 0))
            {
                m_directionsIndex++;

            }

            m_timestepCounter++;
        }

        //USING DEFAULT MOVEMENT - sweeping back and forth
        //If no longer reading instructions, continue default movement
        else
        {
            //MOVE SPACESHIP
            if (m_spaceship->getX() >= xMax)
            {
                m_spaceshipMover *= -1;
            }
            else if (m_spaceship->getX() <= xMin)
            {
                m_spaceshipMover *= -1;
            }
            m_spaceship->moveSpaceshipX(m_spaceshipMover);

            //SHOOT BULLET EVERY 200 PIXELS
            if (m_spaceship->getX() % 200 == 0)
            {
                int bulletX = m_spaceship->getX() + (m_spaceship->getSpaceshipImage().width() / 2) - 10;
                int bulletY = m_spaceship->getY();
                std::string element = "ship";
                m_bullets->addBullet(bulletX, bulletY, element);
            }

        }

        //Increment all active bullets if there are any in action
        if (!m_bullets->isEmpty())
        {
            m_bullets->moveAllBullets();
        }

        //check collisions if both the number of bullets on screen and aliens in grid aren't over
        if (!m_bullets->isEmpty() && !m_aliengrid->isEmpty()) {
            //this->m_aliengrid->checkHits(this->m_bullets->getBullets());
            m_scoreCount += m_collisionHandler->checkCollisions(m_bullets, m_aliengrid, m_spaceship);
        }

        //Move all the starfields
        m_farStarfield->moveStars();
        m_midStarfield->moveStars();
        m_nearStarfield->moveStars();

        //update the frame
        update();

    }

    //Generates a random number to assign images to alien objects
    int Universe::generateRandom(int min, int max)
    {
        if (min != max){
        bool b = true;
        if (b)
        {
            //seeding for the first time only!
           b = false;
        }
        return min + rand() % (max - min);
        }else {
            std::cout << "Game finished!" << std::endl;
            exit(1);
        }
    }
} //end namespace
