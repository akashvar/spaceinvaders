/*
 * angledbullet.h
 * Class header for a bullet that is a subclass of
 * Game Elements. Such bullets travel on a diagonal path.
 * Each instance is given x,y coordinates relative to
 * the spaceship position at the time of instantiation
 * Author: Anon
 *
 */

#ifndef ANGLEDBULLET_H
#define ANGLEDBULLET_H

#include "bullets.h"
#include "gameelement.h"

class AngledBullet: public Bullets
{
public:

    /*
     * Creates a new angled bullet with its passed parameters
     */
    AngledBullet(int xPos, int yPos, int yMover, int xMover);

    //destructor
    virtual ~AngledBullet();

    /*
     * Moves the bullet along the axes
     */
    void moveBullet();

private:
    //can be postive or negative based on which direction the bullet goes
    int m_xMover;
};

#endif // ANGLEDBULLET_H
