/*
 * alien.cpp
 * Class implementation for an alien that is a subclass of
 * Game Elements. It has x, y coordinates and an image (.png)
 */

#include "alien.h"
#include <QString>

Alien::Alien(int x, int y): GameElement(x, y)
{

}

void Alien::moveAlienX(int moveX)
{
    m_xPos += moveX;
}

void Alien::moveAlienY(int moveY)
{
    m_yPos += moveY;
}

void Alien::setImage()
{
    int r = rand() % 4;
    if (r == 0) {
        QPixmap temp_Alien("images/alien1.png");
        m_alienimage = temp_Alien.scaled(QSize(50,50));
    } else if (r == 1) {
        QPixmap temp_Alien("images/alien2.png");
        m_alienimage = temp_Alien.scaled(QSize(50,50));
    } else if (r == 2){
        QPixmap temp_Alien("images/alien3.png");
        m_alienimage = temp_Alien.scaled(QSize(50,50));
    } else {
        QPixmap temp_Alien("images/alien4.png");
        m_alienimage = temp_Alien.scaled(QSize(50,50));
    }
}

QPixmap Alien::getAlienImage() const
{
    return this->m_alienimage;
}
