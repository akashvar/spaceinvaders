/*
 * bullet.h
 * Class header for a bullet that is a subclass of
 * Game Elements. Each instance is given x,y coordinates relative to
 * the spaceship position at the time of instantiation
 * Author: Anon
 *
 */

#ifndef BULLET_H
#define BULLET_H

#include "gameelement.h"
#include "bullets.h"
#include <QPixmap>

class Bullet : public Bullets
{
public:

    /*
     ***** CONSTRUCTOR *****
     * @param x position, y position of the object provided as game element
     * arguments with defaults provided x = -1000, y = -1000
     */
    Bullet(int xPos = -1000, int yPos = -1000, int yMover = 5);

    /*
     ***** DESTRUCTOR *****
     */
    ~Bullet();

    /*
     * Moves the bullet in the y axis
     */
    void moveBullet();

};

#endif // BULLET_H
