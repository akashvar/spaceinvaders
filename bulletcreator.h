/*
 * bulletcreator.h
 * Part of the façade structural pattern.
 * The client (active bullets) only interacts with this class
 * hiding away the complexities of bullets classes.
 *
 */

#ifndef BULLETCREATOR_H
#define BULLETCREATOR_H

#include "bullet.h"
#include "angledbullet.h"

class BulletCreator
{
public:
    BulletCreator();

    /*
     * Creats new angled bullet with the passed parameters
     */
    static AngledBullet *createAngledBullet(int x, int y, int yMover, int xMover);

    /*
     * Creats new bullet with the passed parameters
     */
    static Bullet *createSimpleBullet(int x, int y, int mover);
};

#endif // BULLETCREATOR_H
