/*
 * bullet.cpp
 * Class implementation for a bullet that is a subclass of
 * Game Elements. Each instance is given x,y coordinates relative to
 * the spaceship position at the time of instantiation
 * Author: Anon
 */

#include "bullets.h"
#include "bullet.h"
#include "gameelement.h"


//Constructor loads the bullet image and scales it to 20px by 20px
Bullet::Bullet(int xPos, int yPos, int yMover): Bullets(xPos, yPos, yMover)
{
    //String of file name relative to makefile in the release build
    QPixmap tempBullet("images/Bullet.png");
    m_bulletImage = tempBullet.scaled(QSize(20,20));
}

//Destructor
Bullet::~Bullet()
{

}

//Moves the ship bullet in the y axis by 8px downwards
void Bullet::moveBullet()
{
    m_yPos += m_yMover;
}


